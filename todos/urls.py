from django.urls import path
from todos.views import todo_list_list, todo_list_detail, create_todo_list


urlpatterns = [
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("", todo_list_list, name="todo_list_list"),
    path("create/", create_todo_list, name="todo_list_create"),

]
